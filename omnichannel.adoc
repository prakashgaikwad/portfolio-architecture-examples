= Omnichannel customer experience
Eric D. Schabell @eschabell
:homepage: https://gitlab.com/redhatdemocentral/portfolio-architecture-examples
:imagesdir: images
:icons: font
:source-highlighter: prettify


An omnichannel approach provides a unified customer experience across platforms, creating a single view for
customers to interact with their own information.

Red Hat provides a foundation for IT teams to develop and deliver omnichannel services through a combination
of integration and process automation technologies. Agile integration defines how organizations are transforming
and delivering on their digital promise to customers by integrating applications and services across on-premise
infrastructure and cloud environments. Business automation, in the form of process integrations, are captured to
enable access to complex process services.

Use case: Omnichannel implies integration and orchestration of channels such that the experience of engaging across all the channels
someone chooses to use. 

Open the diagrams in the diagram tooling using 'Load Diagram' link. To download the project file for these diagrams use
the 'Download Diagram' link. The images below can be used to browse the available diagrams and can be embedded into your
content.


--
https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/omnichannel-customer-experience.drawio[[Load Diagram]]
https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/main/diagrams/omnichannel-customer-experience.drawio?inline=false[ [Download Diagram]]
--

--
image:logical-diagrams/omnichannel-customer-experience-ld.png[350,300]
image:logical-diagrams/omnichannel-customer-experience-details-ld.png[350,300]
image:schematic-diagrams/omnichannel-process-integration-sd.png[350,300]
image:schematic-diagrams/omnichannel-mobile-integration-sd.png[350,300]
image:schematic-diagrams/omnichannel-integration-service-sd.png[350,300]
image:schematic-diagrams/omnichannel-integration-data-service-sd.png[350,300]
image:schematic-diagrams/omnichannel-integration-3rd-party-service-sd.png[350,300]
image:schematic-diagrams/omnichannel-process-integration-3rd-party-services-sd.png[350,300]
image:detail-diagrams/mobile-app.png[250,200]
image:detail-diagrams/web-app2.png[250,200]
image:detail-diagrams/api-management2.png[250,200]
image:detail-diagrams/reverse-proxy.png[250,200]
image:detail-diagrams/applications.png[250,200]
image:detail-diagrams/front-end-microservices2.png[250,200]
image:detail-diagrams/process-facade-microservices2.png[250,200]
image:detail-diagrams/integration-microservices2.png[250,200]
image:detail-diagrams/integration-data-microservices2.png[250,200]
image:detail-diagrams/process-server.png[250,200]
image:detail-diagrams/real-time-data-storage.png[250,200]
image:detail-diagrams/sso-server.png[250,200]
--

